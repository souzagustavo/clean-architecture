package usecase

import "gitlab.com/souzagustavo/clean-architecture/internal/entity"

type ListOrdersOutputDTO []OrderOutputDTO

type ListOrdersUseCase struct {
	OrderRepository entity.OrderRepositoryInterface
}

func NewListOrdersUseCase(
	OrderRepository entity.OrderRepositoryInterface,
) *ListOrdersUseCase {
	return &ListOrdersUseCase{
		OrderRepository: OrderRepository,
	}
}

func (lo *ListOrdersUseCase) Execute() (ListOrdersOutputDTO, error) {
	orders, err := lo.OrderRepository.FindAll()
	if err != nil {
		return ListOrdersOutputDTO{}, err
	}

	dtoList := make(ListOrdersOutputDTO, len(orders))

	for index, order := range orders {
		dto := OrderOutputDTO{
			ID:         order.ID,
			Price:      order.Price,
			Tax:        order.Tax,
			FinalPrice: order.Price + order.Tax,
		}
		dtoList[index] = dto
	}

	return dtoList, nil
}
