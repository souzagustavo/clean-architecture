package webserver

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

type Endpoint struct {
	Method  string
	Handler http.HandlerFunc
}

type WebServer struct {
	Router        chi.Router
	Endpoints     map[string][]Endpoint
	WebServerPort string
}

func NewWebServer(serverPort string) *WebServer {
	return &WebServer{
		Router:        chi.NewRouter(),
		Endpoints:     make(map[string][]Endpoint),
		WebServerPort: serverPort,
	}
}

func (s *WebServer) AddHandler(path string, endpoint Endpoint) {
	s.Endpoints[path] = append(s.Endpoints[path], endpoint)
}

// loop through the handlers and add them to the router
// register middeleware logger
// start the server
func (s *WebServer) Start() {
	s.Router.Use(middleware.Logger)
	for path, endpoints := range s.Endpoints {
		for _, endpoint := range endpoints {
			switch endpoint.Method {
			case http.MethodGet:
				s.Router.Get(path, endpoint.Handler)
			case http.MethodPost:
				s.Router.Post(path, endpoint.Handler)
			}
		}
	}
	http.ListenAndServe(s.WebServerPort, s.Router)
}
