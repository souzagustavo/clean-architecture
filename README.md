# GoExpert - Clean Architecture

Pra este desafio, você precisará criar o usecase de listagem das orders.

Esta listagem precisa ser feita com:

- Endpoint REST (GET /order)

- Service ListOrders com GRPC

- Query ListOrders GraphQL

Não esqueça de criar as migrações necessárias e o arquivo api.http com a request para criar e listar as orders.

## Deploy

1) Subir dependências:
```
docker-compose up
```

2) Rodar migrações:
```
make migrate_up
```

3) Subir aplicação:
```
make order_system
```

## Configuração de desenvolvimentos

Comandos dentro do arquivo **Makefile**
