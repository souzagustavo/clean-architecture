create_migration:
	./cmd/migrations/migrate create -ext=sql -dir=./cmd/migrations/sql/migrations -seq init

migrate_up:
	./cmd/migrations/migrate -path=cmd/migrations/sql/migrations -database "mysql://root:root@tcp(localhost:3306)/orders" -verbose up

migrate_down:
	./cmd/migrations/migrate -path=cmd/migrations/sql/migrations -database "mysql://root:root@tcp(localhost:3306)/orders" -verbose down

order_system:
	go mod tidy
	cd cmd/ordersystem; go run main.go wire_gen.go


# CONFIGURAÇÕES

#Download golang-migrate:
# curl -L https://github.com/golang-migrate/migrate/releases/download/v4.16.2/migrate.linux-amd64.tar.gz | tar xvz

#Protocol Buffer Compiler Installation.
protobuf_compiler_install:
	apt install -y protobuf-compiler
	protoc --version

#Install the protocol compiler plugins for Go using the following commands:
protobuf_go_plugins_install:
	go install google.golang.org/protobuf/cmd/protoc-gen-go@v1.28
	go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@v1.2

#Update your PATH so that the protoc compiler can find the plugins:
#$ export PATH="$PATH:$(go env GOPATH)/bin"

#Generate ProtoC Command:
protoc:
	protoc --go_out=. --go-grpc_out=. internal/infra/grpc/protofiles/order.proto

#Generate GraphQL:
gqlgen_generate:
	go run github.com/99designs/gqlgen generate

#instalar client GRPC
grpc_client_install:
	go install github.com/ktr0731/evans@latest

#iniciar client GRPC
grpc_client_start:
	evans -r repl

.PHONY: create_migration migrate_up migrate_down order_system grpc_client_install grpc_client_start protobuf_compiler_install protobuf_go_plugins_install protoc gqlgen_generate
